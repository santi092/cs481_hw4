﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1(Laptop laptop)
        {
            InitializeComponent();
            BindingContext = new Laptop()
            {
                Title = laptop.Title,
                Img = laptop.Img,
                Subtitle = laptop.Subtitle,
                Details = laptop.Details
            };

        }
    }
}