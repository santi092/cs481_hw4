﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public Laptop[] laptops =
        {
            new Laptop()
            {
                Title = "Surface Book2",
                Img = "SB2.jfif",
                Subtitle = "Mostly a laptop that lets you use it like a tablet if you want"
            },
            new Laptop()
            {
                Title = "Surface Pro 7",
                Img = "SP7.jfif",
                Subtitle = "Mostly a tablet that lets you use it like a laptop if you want"
            },
            new Laptop()
            {
                Title = "Surface Laptop 3",
                Img = "SL3.jfif",
                Subtitle = "A laptop"
            }
        };
        public MainPage()
        {
            InitializeComponent();
            ObservableCollection<Laptop> laptopList = new ObservableCollection<Laptop>(laptops);
            x.ItemsSource = laptopList;
        }

        public void Handle_Refreshing(object sender, EventArgs e)
        {
            x.IsPullToRefreshEnabled = false;
        }

        public void Handle_Description(object sender, EventArgs e)
        {

        }
        public void Handle_Delete(object sender, EventArgs e)
        {

        }
    }
}
